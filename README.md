# Output from Repository X-ray

Watch the log for a non-200 http response because that means it is not correctly working as of March 28th.
The kind of file produced in this specific repo looks like:

{
  "scannerVersion": "0.0.1",
  "fileName": "go.mod",
  "checksum": "23b4bc5edf29a80f4010a445d383e993493466e277c16c1c366cd1792b32e8f5",
  "libs": [
    {
      "name": "github.com/joho/godotenv v1.5.1",
      "description": "A Go port of Ruby's dotenv library for loading environment variables from a .env file"
    },
    {
      "name": "github.com/stretchr/testify v1.8.4",
      "description": "A toolkit with common assertions and mocks for testing code in Go "
    },
    {
      "name": "golang.org/x/exp v0.0.0-20240213143201-ec583247a57a",
      "description": "The exp repository holds experimental Go packages"
    },
    {
      "name": "gopkg.in/yaml.v3 v3.0.1",
      "description": "Implements YAML support for the Go language"
    }
  ]
}


# Repository X-Ray

Repository X-Ray is a tool designed to scan a specified project directory for configuration files associated with various package managers.
It extracts information about configured dependencies from these package managers.
Once the list of dependencies is obtained, X-Ray fetches detailed descriptions for each dependency.
After collecting descriptions for all dependencies, X-Ray compiles and stores the list along with their descriptions in report files.
These report files are organized based on the programming language of the project.

Generated reports are used later by [GitLab Duo Code Suggestions](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/index.html) by providing additional context to improve the accuracy and relevance of code recommendations.


## Supported languages and package managers

| Language   | Package Manager | Configuration File   |
| ---------- |-----------------| -------------------- |
| Go         | Go Modules      | `go.mod`             |
| JavaScript | NPM, Yarn       | `package.json`       |
| Ruby       | RubyGems        | `Gemfile.lock`       |
| Python     | Poetry          | `pyproject.toml`     |
| Python     | Pip             | `requirements.txt`   |
| Python     | Conda           | `environment.yml`    |
| PHP        | Composer        | `composer.json`      |
| Java       | Maven           | `pom.xml`            |
| Java       | Gradle          | `build.gradle`       |
| Kotlin     | Gradle          | `build.gradle.kts`   |
| C#         | NuGet           | `*.csproj`           |
| C/C++      | Conan           | `conanfile.txt`      |
| C/C++      | Conan           | `conanfile.py`       |
| C/C++      | vcpkg           | `vcpkg.json`         |


### Known issues

#### `conanfile.py`

Using `conanfile.py` as a configuration file for the Conan package manager provides a lot of freedom in structuring the list of dependencies.
However, not every possible way of structuring the dependencies list is supported.

For example, the following formats are considered correct and will be discovered by X-Ray scan:

```python
requires = (
    # Regular dependencies
    "boost/1.76.0",
    "opencv/4.6.0",
    "poco/1.11.0",

    # Test dependencies
    "gtest/1.11.0",
    "catch2/3.0.0"
)

# ...

def requirements(self):
    self.requires("zlib/1.2.11")
    self.requires("eigen/3.3.9@conan/stable")
```

However, examples such as the ones below won't be recognized:

```python
requires = ("boost/1.76.0", "opencv/4.6.0", "poco/1.11.0")

# or

requires = ("boost/1.76.0",
  "opencv/4.6.0", "poco/1.11.0")

# or

requires = ("boost/1.76.0", "opencv/4.6.0",
    "poco/1.11.0", "gtest/1.11.0",
    "catch2/3.0.0")
```

## Development setup

1. (Optional) The project uses `golangci-lint` as a linter. Which is also the part of `make audit` task. Please refer to [installation instructions](https://golangci-lint.run/usage/install/#local-installation) to install it.

## How to run locally

1. [Run the AI Gateway server](https://gitlab.com/gitlab-org/modelops/applied-ml/code-suggestions/ai-assist#how-to-run-the-server-locally) locally
1. Make sure `gdk` [is running locally](https://gitlab.com/gitlab-org/gitlab-development-kit#local)
  1. Make sure you have the following environment variables set before `gdk start`
    ```
    export GITLAB_SIMULATE_SAAS=1
    export AI_GATEWAY_URL=http://0.0.0.0:5052
    ```
1. Prepare a job on localhost
    1. Run `bundle exec rails c` from your `gdk/rails` directory
    1. Run the following Ruby code:
        ```ruby
        build = Ci::Build.first
        build.set_token 'secret'
        build.status = "running"
        build.save
        namespace = build.project.namespace
        FactoryBot.create(:gitlab_subscription_add_on_purchase, add_on: FactoryBot.create(:gitlab_subscription_add_on), namespace: namespace)
        build.id
        ```
    1. Exit the console `exit`
    1. Optionally you can check if everything is working on the Rails side:
        ```
        curl -v -X POST -H "Content-Type: Application/json"  http://localhost:3000/api/v4/internal/jobs/1/x_ray/scan --data '{
          "token": "secret",
          "prompt_components": [
            {
              "type":"x_ray_package_file_prompt",
              "payload":{
                "prompt": "\n\nHuman: What is your model name? \n\n Assistant:",
                "provider": "anthropic",
                "model": "claude-2.0"
              },
              "metadata": { "scannerVersion": "0.0.1" }
            }
          ]
        }'
        ```

        you should see a response similar to

        ```
        {"response":" I don't have a specific model name. I'm Claude, an AI assistant created by Anthropic."}
        ```
1. Run `cp .env.example .env` in the X-Ray directory
1. Set the required variables in `.env`
    1. Set `CI_API_V4_URL` to your localhost url
    1. Set `CI_JOB_ID` to `build.id` from previous steps
    1. Set `CI_JOB_TOKEN` to `secret` (or to a token used in `build.set_token 'secret'` above)

### Troubleshooting

#### `WARN Failed to get completions. Skipping batch. GitLab API returned non-200 status code: 403`

The X-Ray scanner is designed to run inside of GitLab Rails CI job and uses the CI job token mechanism. In order to run it on local, with a gdk instance, it is required to emulate running job. Snippet below provides a way to do so via Rails console.

1. Run `bundle exec rails c` from your `gdk/rails` directory
1. Run the following Ruby code again:
    ```ruby
    build = Ci::Build.first
    build.set_token 'secret'
    build.status = "running"
    build.save
    namespace = build.project.namespace
    build.id
    ```

#### `WARN Failed to get completions. Skipping batch. GitLab API returned non-200 status code: 404`

Make sure `gdk` is running as SaaS. Please refer to [Simulate a SaaS instance](https://docs.gitlab.com/ee/development/ee_features.html#simulate-a-saas-instance) documentation page for more details.

```
export GITLAB_SIMULATE_SAAS=1
```

## Dogfooding

This repository itself uses X-Ray scan feature by defining `xray_scan` job in its `.gitlab-ci.yml` config file. It is possible to preview raw content of X-Ray scan report by viewing `xray_scan` job attached artifacts. You can read more about `artifacts: reports` and CI job configuration at [dedicated GitLab documentation page](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsrepository_xray).

## Release Process

In order to provide more control over the release process that distributes changes to the X-Ray scanner to GitLab users, this repository follows the release process defined below:

1. On feature branches, `build` CI jobs create `registry.gitlab.com/gitlab-org/code-creation/repository-x-ray/dev:$CI_COMMIT_SHORT_SHA` images that are used for end-to-end testing purposes.

2. To release a new version of the scanner, a new [Git tag](https://docs.gitlab.com/ee/user/project/repository/tags/#create-a-tag) must be created.

3. The `build` CI job that runs on Git tags creates the `registry.gitlab.com/gitlab-org/code-creation/repository-x-ray` image and tags it as `rc`.

4. Images with the `rc` tag are used by [`gitlab-org/gitlab`](https://gitlab.com/gitlab-org/gitlab) and this project for testing. 

5. After one business day of using the new `rc` image, a manual `release` job is created that tags the `rc` image as `latest` to make it available to all GitLab users.

## Versioning

This project follows the [SemVer](https://semver.org) specification.

## Roadmap

The overarching idea behind the repository X-Ray feature is to build comperhensive [RAG](https://www.promptingguide.ai/techniques/rag) tool that will provide relevant context information sourced from the repository (e.g. code snippets) for code creation requests.


To track progress on this project, you can refer to [this epic](https://gitlab.com/groups/gitlab-org/-/epics/11733) that captures the high-level vision and open issues that represent smaller iterations towards that goal.

## Contributing

See [CONTRIBUTING](CONTRIBUTING.md) file.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License

See [LICENSE](LICENSE)
