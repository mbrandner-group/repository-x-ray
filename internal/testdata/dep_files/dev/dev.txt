# Python pip requirements.txt example
fastapi==0.104.1        # Mathcing specific version
detect-secrets>=1.4.0   # Minimum version

fastapi-health!=0.3.0   # Exclusion
tree-sitter~=0.20.4     # Compatible version
anthropic == 0.7.7      # Matching specific version with extra spaces

uvicorn                 # No version specified
python-dotenv
pytest==7.2.0
