[requires]
boost/1.76.0
opencv/4.6.0
fmt/8.0.1
zlib/1.2.11
openssl/1.1.1l
sqlite3/3.37.0 # DB dependency

# Test dependencies
gtest/1.11.0

[generators]
cmake