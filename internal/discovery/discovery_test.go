package discovery

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestLocateFile(t *testing.T) {
	tests := []struct {
		name      string
		dir       string
		fileName  string
		wantPaths []string
		wantError bool
	}{
		{"with Gemfile.lock", "../testdata/dep_files", "Gemfile.lock", []string{"../testdata/dep_files/Gemfile.lock"}, false},
		{"without Gemfile.lock", "../testdata/empty", "Gemfile.lock", []string{}, false},
		{"with package.json", "../testdata/dep_files", "package.json", []string{"../testdata/dep_files/package.json"}, false},
		{"without package.json", "../testdata/empty", "package.json", []string{}, false},
		{"with go.mod", "../testdata/dep_files", "go.mod", []string{"../testdata/dep_files/go.mod"}, false},
		{"without go.mod", "../testdata/empty", "go.mod", []string{}, false},
		{"without go.mod", "../testdata/doesnotexist", "go.mod", []string{}, true},
		{"with ConsoleApp1.csproj", "../testdata/dep_files", "ConsoleApp1.csproj", []string{"../testdata/dep_files/ConsoleApp1/ConsoleApp1.csproj"}, false},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotPaths, err := LocateFile(tt.dir, tt.fileName)

			require.Equal(t, tt.wantPaths, gotPaths)
			require.Equal(t, tt.wantError, err != nil)
		})
	}
}

func TestFileChecksum(t *testing.T) {
	tests := []struct {
		name string
		path string
		want string
	}{
		{
			"with Gemfile.lock",
			"../testdata/dep_files/Gemfile.lock",
			"9d54354092027c20112dd0852cc4ded7c0713f43719eb58fc7a92342942e5437",
		},
		{
			"with package.json",
			"../testdata/dep_files/package.json",
			"26726759188d1ed8e7fcfdc72f9b4ca1b914fe10fc621cb472e529ad71f4ff79",
		},
		{
			"with no file",
			"../testdata/dep_files/no-file",
			"",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := FileChecksum(tt.path)

			require.Equal(t, tt.want, got)
		})
	}
}
