package discovery

import (
	"crypto/sha256"
	"encoding/hex"
	"io/fs"
	"os"
	"path"
	"path/filepath"

	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/deps"
)

type DependencyFile struct {
	FileName  string
	Found     bool
	FoundPath string
	DirPath   string
	DepType   deps.Type
}

func LocateFile(dir string, fileName string) ([]string, error) {
	matches := []string{}

	err := filepath.Walk(dir, func(walkfile string, info fs.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			fileMatches, err := filepath.Glob(path.Join(walkfile, fileName))
			if err != nil {
				return err
			}
			matches = append(matches, fileMatches...)
		}
		return nil
	})

	return matches, err
}

func FileChecksum(filePath string) string {
	data, err := os.ReadFile(filePath)
	if err != nil {
		return ""
	}

	hash := sha256.Sum256(data)

	return hex.EncodeToString(hash[:])
}
