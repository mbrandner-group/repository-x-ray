package prompt

import (
	"bytes"
	"embed"
	"errors"
	"text/template"

	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/deps"
)

//go:embed description_prompt.tmpl
var tmplContent embed.FS

// ErrNoLibs is returned when no libs are provided to prompt generator
var ErrNoLibs = errors.New("no libs provided")

var (
	descriptionSeparator = "---"
	ResponseClosingTag   = "</description>"
)

type Prompt struct{}

type PromptData struct {
	LibDesc   string
	Libs      []deps.Dependency
	Separator string
}

func New() *Prompt {
	return &Prompt{}
}

func (p *Prompt) LibsDescription(libs []deps.Dependency, libType deps.Type) (string, error) {
	if len(libs) == 0 {
		return "", ErrNoLibs
	}

	d := PromptData{
		LibDesc:   deps.TypeDescription(libType),
		Libs:      libs,
		Separator: descriptionSeparator,
	}

	tmplFile := "description_prompt.tmpl"
	tmplData, err := tmplContent.ReadFile(tmplFile)
	if err != nil {
		return "", err
	}

	tmpl, err := template.New(tmplFile).Parse(string(tmplData))
	if err != nil {
		return "", err
	}

	var res bytes.Buffer

	err = tmpl.Execute(&res, d)
	if err != nil {
		return "", err
	}

	return res.String(), nil
}
