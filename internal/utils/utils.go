package utils

import "math"

// EachBatch iterates over a slice in batches of size batchSize and calls fn with each batch
func EachBatch[T any](elems []T, batchSize int, fn func(batch []T, totalBatches int, currentBatch int)) {
	totalBatches := int(math.Ceil(float64(len(elems)) / float64(batchSize)))
	currentBatch := 0

	for i := 0; i < len(elems); i += batchSize {
		currentBatch++

		end := i + batchSize
		if end > len(elems) {
			end = len(elems)
		}
		fn(elems[i:end], totalBatches, currentBatch)
	}
}
