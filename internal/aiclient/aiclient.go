package aiclient

import (
	"strings"

	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/deps"
)

type AIClient interface {
	Completions(prompt string) ([]deps.Dependency, error)
}

func parseBody(body string) []deps.Dependency {
	r := []deps.Dependency{}
	lines := strings.Split(body, "\n")
	for _, line := range lines {
		parts := strings.Split(line, " --- ")
		if len(parts) != 2 {
			continue
		}

		r = append(r, deps.Dependency{
			Name:        parts[0],
			Description: parts[1],
		})

	}

	return r
}
