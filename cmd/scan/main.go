package main

import (
	"flag"
	"fmt"
	"log/slog"
	"os"
	"path/filepath"
	"strings"

	"github.com/joho/godotenv"
	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/aiclient"
	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/deps"
	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/discovery"
	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/prompt"
	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/report"
	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/scanner"
	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/utils"
	"golang.org/x/exp/maps"
	"golang.org/x/mod/semver"
)

const (
	version            = "1.1.0"
	fetchDescBatchSize = 25
)

var (
	build     string
	buildTime string

	dependencies = []*discovery.DependencyFile{
		{FileName: "go.mod", DepType: deps.Go},
		{FileName: "package.json", DepType: deps.JavaScript},
		{FileName: "Gemfile.lock", DepType: deps.Ruby},
		{FileName: "pyproject.toml", DepType: deps.PythonPoetry},
		{FileName: "requirements.txt", DepType: deps.PythonPip},
		{FileName: "environment.yml", DepType: deps.PythonConda},
		{FileName: "composer.json", DepType: deps.PHP},
		{FileName: "pom.xml", DepType: deps.JavaMaven},
		{FileName: "build.gradle", DepType: deps.JavaGradle},
		{FileName: "build.gradle.kts", DepType: deps.KotlinGradle},
		{FileName: "*.csproj", DepType: deps.CSharp},
		{FileName: "conanfile.txt", DepType: deps.CppConanTxt},
		{FileName: "conanfile.py", DepType: deps.CppConanPy},
		{FileName: "vcpkg.json", DepType: deps.CppVcpkg},
	}

	dstFiles = map[deps.Type]string{
		deps.Go:           "go.json",
		deps.JavaScript:   "javascript.json",
		deps.Ruby:         "ruby.json",
		deps.PythonPoetry: "python.json",
		deps.PythonPip:    "python.json",
		deps.PythonConda:  "python.json",
		deps.PHP:          "php.json",
		deps.JavaMaven:    "java.json",
		deps.JavaGradle:   "java.json",
		deps.KotlinGradle: "kotlin.json",
		deps.CSharp:       "csharp.json",
		deps.CppConanTxt:  "cpp.json",
		deps.CppConanPy:   "cpp.json",
		deps.CppVcpkg:     "cpp.json",
	}
)

type application struct {
	metadata struct {
		buildTime string
		version   string
	}
	aiclientData struct {
		apiV4Url string
		jobID    string
		token    string
	}
	scanDir        string
	reportsDir     string
	displayVersion bool
}

func main() {
	app := NewApplication()
	app.Init()
	app.Run()
	app.Done()
}

// NewApplication creates a new application instance and sets initial values
func NewApplication() *application {
	app := application{}
	app.metadata.buildTime = buildTime
	app.metadata.version = fmt.Sprintf("%s+%s", version, build)

	return &app
}

// Init initializes the application by parsing command line arguments and loading environment variables
func (app *application) Init() {
	_ = godotenv.Load()

	dir, err := os.Getwd()
	if err != nil {
		slog.Error("Failed to get current working directory", err)
		os.Exit(1)
	}
	flag.StringVar(&app.scanDir, "p", dir, "Path to the directory to scan")
	flag.StringVar(&app.reportsDir, "o", "reports", "Path to the directory to store scan reports")
	flag.BoolVar(&app.displayVersion, "version", false, "Print app version")

	flag.Func("additionalPipRequirements", "Additional requirements files for python pip (i.e. dev.txt,test.txt)", func(flagValue string) error {
		fileNames := strings.Split(flagValue, ",")
		for _, f := range fileNames {
			dependencies = append(dependencies, &discovery.DependencyFile{FileName: f, DepType: deps.PythonPip})
		}

		return nil
	})
	flag.Parse()

	app.aiclientData.apiV4Url = os.Getenv("CI_API_V4_URL")
	app.aiclientData.jobID = os.Getenv("CI_JOB_ID")
	app.aiclientData.token = os.Getenv("CI_JOB_TOKEN")
	err = app.validateAIClientData()
	if err != nil {
		slog.Error(fmt.Sprintf("Failed to create GitLab client: %v", err))
		os.Exit(1)
	}
}

// Run starts the application by scanning for dependencies and creating reports
func (app *application) Run() {
	if app.displayVersion {
		app.printVersionInfo()
		os.Exit(0)
	}

	slog.Info("Scanning in " + app.scanDir)

	foundDependencies := app.findDependencies(dependencies)

	var depsByType map[deps.Type][]*discovery.DependencyFile = make(map[deps.Type][]*discovery.DependencyFile)

	for _, dep := range foundDependencies {
		depsByType[dep.DepType] = append(depsByType[dep.DepType], dep)
	}

	for _, depList := range depsByType {
		app.processDependencies(depList)
	}
}

// Done wraps up the application work and can perform any cleanup if necessary
func (app *application) Done() {
	slog.Info("X-Ray scan finished")
}

func (app *application) validateAIClientData() error {
	if app.aiclientData.apiV4Url == "" {
		return fmt.Errorf("GitLab API V4 URL is missing")
	}

	if app.aiclientData.jobID == "" {
		return fmt.Errorf("GitLab CI Job ID is missing")
	}

	if app.aiclientData.token == "" {
		return fmt.Errorf("GitLab CI Token is missing")
	}

	return nil
}

func (app *application) findDependencies(depFiles []*discovery.DependencyFile) []*discovery.DependencyFile {
	foundDependencies := []*discovery.DependencyFile{}
	for _, dep := range depFiles {
		foundFiles, err := discovery.LocateFile(app.scanDir, dep.FileName)
		if err != nil {
			slog.Debug(fmt.Sprintf("Error while locating file %s - %s", dep.FileName, err))
		}

		for _, file := range foundFiles {
			foundDependencies = append(foundDependencies, &discovery.DependencyFile{
				Found:     true,
				FoundPath: file,
				DirPath:   app.scanDir,
				FileName:  filepath.Base(file),
				DepType:   dep.DepType,
			})
			slog.Info(fmt.Sprintf("Found %s at %s", filepath.Base(file), filepath.Dir(file)))
		}
	}
	return foundDependencies
}

func (app *application) processDependencies(depList []*discovery.DependencyFile) {
	if len(depList) == 0 {
		return
	}

	var rprt *report.Report
	var foundDeps map[string]deps.Dependency = make(map[string]deps.Dependency)

	depType := depList[0].DepType
	dd := deps.TypeDescription(depType)
	checksum := discovery.FileChecksum(depList[0].FoundPath)
	rprt = report.New(app.reportsDir, dstFiles[depType], version, depList[0].FileName, checksum)

	for _, dep := range depList {
		displayPath, err := filepath.Rel(app.scanDir, dep.FoundPath)
		if err != nil {
			displayPath = dep.FoundPath
		}
		slog.Info(fmt.Sprintf("Scanning %s", displayPath))
		myFoundDeps, _ := scanner.Scan(dep.FoundPath, dep.DepType)
		for _, d := range myFoundDeps {
			_, exists := foundDeps[d.Name]
			if !exists || semver.Compare(d.Version, foundDeps[d.Name].Version) == 1 {
				foundDeps[d.Name] = d
			}
		}

		slog.Info(fmt.Sprintf("Found %d %s", len(myFoundDeps), dd))
	}

	if len(foundDeps) == 0 {
		return
	}

	p := prompt.New()
	client := aiclient.NewGitLab(
		rprt.ScannerVersion,
		app.aiclientData.apiV4Url,
		app.aiclientData.jobID,
		app.aiclientData.token,
	)

	slog.Info(fmt.Sprintf("Getting %s descriptions. This can take a while...\n", dd))

	rprt.Libs = make([]deps.Dependency, 0, len(foundDeps))
	utils.EachBatch(maps.Values(foundDeps), fetchDescBatchSize, func(batch []deps.Dependency, totalBatches int, currentBatch int) {
		slog.Info(fmt.Sprintf("Processing batch... (%d/%d)\n", currentBatch, totalBatches))

		prmt, err := p.LibsDescription(batch, depType)
		if err != nil {
			slog.Warn(fmt.Sprintf("failed to prepare prompt for %s: %v", deps.TypeDescription(depType), err))
			return
		}

		resp, err := client.Completions(prmt)
		if err != nil {
			slog.Warn(fmt.Sprintf("Failed to get completions. Skipping batch. %v\n", err))
			return
		}

		rprt.Libs = append(rprt.Libs, resp...)
	})

	err := rprt.Save()
	if err != nil {
		slog.Error(fmt.Sprintf("Failed to save report: %v", err))
		return
	}
}

func (app *application) printVersionInfo() {
	fmt.Printf("Version: %s\n", app.metadata.version)
	fmt.Printf("Build time: %s\n", app.metadata.buildTime)
}
