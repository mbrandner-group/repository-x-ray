package main

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/deps"
	"gitlab.com/gitlab-com/create-stage/code-creation/repository-x-ray/internal/discovery"
)

func TestValidateAIClientData(t *testing.T) {
	tests := []struct {
		name     string
		apiV4Url string
		jobID    string
		token    string
		wantErr  bool
	}{
		{"valid data", "http://127.0.0.1:3000/api/v4", "1", "secret", false},
		{"url missing", "", "1", "secret", true},
		{"jobID missing", "http://127.0.0.1:3000/api/v4", "", "secret", true},
		{"token missing", "http://127.0.0.1:3000/api/v4", "1", "", true},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			testApp := application{
				aiclientData: struct {
					apiV4Url string
					jobID    string
					token    string
				}{tt.apiV4Url, tt.jobID, tt.token},
			}

			gotErr := testApp.validateAIClientData()
			if tt.wantErr {
				require.Error(t, gotErr)
			} else {
				require.NoError(t, gotErr)
			}
		})
	}
}

const internalTestData = "../../internal/testdata/dep_files"

func TestFindDependencies(t *testing.T) {
	tests := []struct {
		name     string
		scanDir  string
		deps     []*discovery.DependencyFile
		expected []*discovery.DependencyFile
	}{
		{
			"single test",
			internalTestData,
			[]*discovery.DependencyFile{
				{FileName: "requirements.txt", DepType: deps.PythonPip},
			},
			[]*discovery.DependencyFile{
				{FileName: "requirements.txt", DirPath: internalTestData, DepType: deps.PythonPip},
			},
		},
		{
			"custom test",
			internalTestData,
			[]*discovery.DependencyFile{
				{FileName: "requirements.txt", DepType: deps.PythonPip},
				{FileName: "dev.txt", DirPath: internalTestData, DepType: deps.PythonPip},
			},
			[]*discovery.DependencyFile{
				{FileName: "requirements.txt", DepType: deps.PythonPip},
				{FileName: "dev.txt", DirPath: internalTestData + "/dev", DepType: deps.PythonPip},
			},
		},
		{
			"multi type test",
			internalTestData,
			[]*discovery.DependencyFile{
				{FileName: "requirements.txt", DepType: deps.PythonPip},
				{FileName: "package.json", DepType: deps.JavaScript},
				{FileName: "Gemfile.lock", DepType: deps.Ruby},
			},
			[]*discovery.DependencyFile{
				{FileName: "requirements.txt", DepType: deps.PythonPip},
				{FileName: "package.json", DirPath: internalTestData, DepType: deps.JavaScript},
				{FileName: "Gemfile.lock", DirPath: internalTestData, DepType: deps.Ruby},
			},
		},
		{
			"multi file test",
			internalTestData,
			[]*discovery.DependencyFile{
				{FileName: "requirements.txt", DepType: deps.PythonPip},
				{FileName: "multi.txt", DepType: deps.JavaScript},
			},
			[]*discovery.DependencyFile{
				{FileName: "requirements.txt", DepType: deps.PythonPip},
				{FileName: "multi.txt", DirPath: internalTestData, DepType: deps.JavaScript},
				{FileName: "multi.txt", DirPath: internalTestData + "/dev", DepType: deps.JavaScript},
			},
		},
	}

	app := NewApplication()

	for _, tt := range tests {
		app.scanDir = tt.scanDir
		t.Run(tt.name, func(t *testing.T) {
			foundDeps := app.findDependencies(tt.deps)

			require.Greater(t, len(foundDeps), 0)

			for ndx, exp := range tt.expected {
				dep := foundDeps[ndx]
				require.Equal(t, dep.DepType, exp.DepType)
				require.Equal(t, dep.Found, true)
				require.Equal(t, dep.DirPath, internalTestData)
				require.Equal(t, dep.FileName, exp.FileName)
			}
		})
	}
}
